Drupal Google Fonts per Theme module:
----------------------------------
Maintainers:
  Mark Wise (http://drupal.org/user/501326)
Requires - Drupal 6
License - GPL (see LICENSE)


Overview:
--------
This module allows theme developers to specify google fonts in their theme's .info file.  For more full-featured GFonts support (e.g. admin UI), check out the Google Fonts module (http://drupal.org/project/google_fonts) by BarisW.


Installation: 
------------
1. Download and enable the module as usual

How it works:
------------

This module will scan your theme's .info file and selectively load fonts using the Google Font API.


Example usage:
-------------
In your theme's .info file:

fonts[] = 'Crimson Text'
fonts[] = 'Nobile:b,i,bi'
fonts[] = 'Josefin Slab:regular,regularitalic,bold,bolditalic'
fonts[] = 'Arvo:regular,bold,italic,bolditalic'

For in-depth documentation, visit http://code.google.com/apis/webfonts/docs/getting_started.html#Quick_Start